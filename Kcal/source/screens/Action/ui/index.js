import {View, Text} from 'react-native';
import React from 'react';
import styles from './styles';

const Action = () => {
  return (
    <View style={styles.container}>
      <Text>Action</Text>
    </View>
  );
};

export default Action;
