import {View, Text, Button} from 'react-native';
import React from 'react';
import styles from './styles';

const Login = () => {
  return (
    <View style={styles.container}>
      <Text>Login</Text>
      <Button
        title="Login"
        onPress={() => {
          console.log(global.isLogin);
        }}
      />
    </View>
  );
};

export default Login;
