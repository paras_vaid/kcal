export default {
  primary: '#F5F5F5',
  secondary: '#1B4633',
  secondaryShade: '#889E94',
  accent: '#9EE0D9',
  white: '#FFFFFF',
  whiteShade: '#FAFAFA',
  pink: '#F7BAB4',
  yellow: '#F7CF81',
  grey: '#D4D4D4',
};
