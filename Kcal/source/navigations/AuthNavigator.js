import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {LOGIN} from '../constants/screens';
import Login from '../screens/Login/ui';

const Stack = createNativeStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={LOGIN} component={Login} />
    </Stack.Navigator>
  );
};

export default AuthNavigator;
