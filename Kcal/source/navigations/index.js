import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import HomeNavigator from './HomeNavigator';
import AuthNavigator from './AuthNavigator';

const NavContainer = () => {
  const [isLogin, setIsLogin] = useState(true);

  return (
    <NavigationContainer>
      {isLogin ? <HomeNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  );
};

export default NavContainer;
