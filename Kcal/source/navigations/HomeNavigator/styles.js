import {StyleSheet} from 'react-native';
import colors from '../../assets/themes/colors';

export default StyleSheet.create({
  container: {
    borderRadius: 30,
    position: 'absolute',
    width: '90%',
    justifyContent: 'center',
    bottom: '5%',
    alignSelf: 'center',
  },
  indicator: {
    backgroundColor: 'transparent',
  },
  iconContainer: {
    alignItems: 'center',
  },
  iconLable: {
    marginTop: 5,
    color: colors.secondary,
  },
  iconLableFocused: {
    fontWeight: 'bold',
  },
  icon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    tintColor: colors.secondary,
  },
  tabBarContentContainerStyle: {
    marginVertical: 15,
  },
  tabBarIconStyle: {
    width: 80,
    justifyContent: 'center',
  },
});
