import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Image, Text, View} from 'react-native';
import Home from '../../screens/Home/ui';
import Profile from '../../screens/Profile/ui';
import {ACTION, HOME, PROFILE} from '../../constants/screens';
import icons from '../../assets/icons';
import Action from '../../screens/Action/ui';
import styles from './styles';

const Tab = createMaterialTopTabNavigator();

const HomeNavigator = () => {
  const tabIcons = (route, focused) => {
    if (route.name === HOME) {
      return (
        <View style={styles.iconContainer}>
          <Image style={styles.icon} source={icons.home} />
          <Text style={[styles.iconLable, focused && styles.iconLableFocused]}>
            {route.name}
          </Text>
        </View>
      );
    } else if (route.name === PROFILE) {
      return (
        <View style={styles.iconContainer}>
          <Image style={styles.icon} source={icons.profile} />
          <Text style={[styles.iconLable, focused && styles.iconLableFocused]}>
            {route.name}
          </Text>
        </View>
      );
    } else if (route.name === ACTION) {
      return (
        <View style={styles.iconContainer}>
          <Image style={styles.icon} source={icons.action} />
          <Text style={[styles.iconLable, focused && styles.iconLableFocused]}>
            {route.name}
          </Text>
        </View>
      );
    }
  };
  return (
    <Tab.Navigator
      initialRouteName={HOME}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => tabIcons(route, focused),
        tabBarIndicatorStyle: styles.indicator,
        tabBarStyle: styles.container,
        tabBarContentContainerStyle: styles.tabBarContentContainerStyle,
        tabBarIconStyle: styles.tabBarIconStyle,
        tabBarShowLabel: false,
      })}>
      <Tab.Screen name={ACTION} component={Action} />
      <Tab.Screen name={HOME} component={Home} />
      <Tab.Screen name={PROFILE} component={Profile} />
    </Tab.Navigator>
  );
};

export default HomeNavigator;
